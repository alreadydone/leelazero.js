#!/usr/bin/env python3

# import the Flask class from the flask module
from flask import Flask, render_template, redirect

# create the application object
app = Flask(__name__)

# use decorators to link the function to a url
@app.route('/')
def home():
    return redirect('/static/jgoboard/leelazero_test.html')

@app.route('/welcome')
def welcome():
	return render_template('welcome.html')  # render a template

# start the server with the 'run()' method
if __name__ == '__main__':
	app.run(debug=True, host="0.0.0.0")