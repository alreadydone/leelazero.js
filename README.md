# LeelaZero.js

- [x] Export Model to Keras
- [X] Export Keras to Tensorflow.js
- [X] .js Board Encoder to 18 Planes
- [X] Live 1 playout version, i.e. let bot play move with highest Policy valie
- [ ] Basic MC
- [ ] Other MC ?
- [ ] Full MCTS ?

## Online Demo

Play against the `10x128` single playout network. Wait for the network to load, on slow connections this might take a while.

https://www.alphago-games.com/static/jgoboard/leelazero_test.html

## Build a LeelaZero.js yourself

### LeelaZero to Keras

Use https://gitlab.com/whendrik/leelazerotokeras/ to export a LeelaZero network to Keras. The output will be a file like `leelazero_1x8.h5`.

Make sure the Model is exported with nightly 2.0, `pip install -U tf-nightly-2.0-preview` (https://github.com/tensorflow/tfjs/issues/1255).

### Keras to Tensorflow.js 

With `leelazero_1x8.h5` being a output of LeelaZero to Keras Conversion

```
pip install tensorflowjs

tensorflowjs_converter --input_format keras leelazero_1x8.h5 leelazero_js_output
```

## Call in Javascript

Make sure to refresh the browsers cache when testing new models, Chrome is very persistent with JavaScript Cache.

```
import * as tf from '@tensorflow/tfjs';

const model = await tf.loadLayersModel('http://127.0.0.1:5000/static/leelazero_js_output/model.json');

empty_board = tf.zeros([1,19,19,18])

preds = model.predict(empty_board)

policy_out = preds[0]
value_out = preds[1]

policy_out.print()
policy_out.array()

```